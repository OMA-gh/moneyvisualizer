﻿using MoneyVisualizer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MoneyVisualizer
{
    class Payment : ViewModelBase
    {
        private uint _year;
        private uint _month;
        private uint _day;
        private int _price;
        private string _usage;
        private PaymentType _type;

        public uint Year
        {
            get
            {
                return _year;
            }
            set
            {
                _year = value;
                RaisePropertyChanged("Year");
            }
        }
        public uint Month { get
            {
                return _month;
            }
            set
            {
                _month = value;
                RaisePropertyChanged("Month");
            }
        }
        public uint Day { get
            {
                return _day;
            }
            set
            {
                _day = value;
                RaisePropertyChanged("Day");
            }
        }

        public int Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                RaisePropertyChanged("Price");
            }
        }
        public string Usage
        {
            get
            {
                return _usage;
            }
            set
            {
                _usage = value;
                RaisePropertyChanged("Usage");
            }
        }
        public PaymentType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                RaisePropertyChanged("Type");
            }
        }
        public PaymentType[] PaymentTypes { get; set; }

        public void convertDate(string csv_date)
        {
            //2018年12月1日
            string pattern = @"(\w*)年(\w*)月(\w*)日";
            Regex r = new Regex(pattern, RegexOptions.IgnoreCase);

            Match m = r.Match(csv_date);
            if (m.Success)
            {
                Year  = uint.Parse(m.Groups[1].ToString());
                Month = uint.Parse(m.Groups[2].ToString());
                Day   = uint.Parse(m.Groups[3].ToString());
            }
        }
    }
}
