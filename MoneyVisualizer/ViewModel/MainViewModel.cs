﻿using MoneyVisualizer.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;

namespace MoneyVisualizer.ViewModel
{
    class MainViewModel : ViewModelBase
    {
        private ObservableCollection<Payment> _paymentCollection;
        private Payment _selectData;
        public Collection<Payment> OutputCollection{get;set;}

        public ObservableCollection<Payment> PaymentCollection
        {
            get
            {
                return _paymentCollection;
            }
            set
            {
                _paymentCollection = value;
                RaisePropertyChanged("PaymentCollection");
            }
        }
        public Payment SelectData
        {
            get
            {
                return _selectData;
            }
            set
            {
                _selectData = value;
                RaisePropertyChanged("SelectData");
            }
        }

        public DelegateCommand OutputYaml { get; private set; }
        public DelegateCommand ReloadCommand { get; private set; }
        public PaymentType[] PaymentTypes { get; set; } = (PaymentType[])Enum.GetValues(typeof(PaymentType));

        public MainViewModel()
        {
            OutputYaml = new DelegateCommand(outputYaml);
            ReloadCommand = new DelegateCommand(reload);
            PaymentCollection = readYML();
        }

        ObservableCollection<Payment> readYML()
        {
            var ret_collection = new ObservableCollection<Payment>();
            string yml_path = @"Resource\yaml\CreditYaml.yml";

            YamlStream yaml = new YamlStream();
            using (var input = new StreamReader(yml_path, Encoding.UTF8))
            {
                yaml.Load(input);
            }
            YamlDocument doc = yaml.Documents[0];
            YamlSequenceNode root = doc.RootNode as YamlSequenceNode;
            if (root != null)
            {
                foreach (var node in root)
                {
                    var mapping = node as YamlMappingNode;
                    if (mapping != null)
                    {
                        var add_payment = new Payment();
                        add_payment.Usage = mapping["Usage"].ToString();
                        add_payment.Price = int.Parse(mapping["Price"].ToString());
                        add_payment.Year = uint.Parse(mapping["Year"].ToString());
                        add_payment.Month = uint.Parse(mapping["Month"].ToString());
                        add_payment.Day = uint.Parse(mapping["Day"].ToString());
                        PaymentType type = PaymentType.None;
                        Enum.TryParse<PaymentType>(mapping["Type"].ToString(), out type);
                        add_payment.Type = type;
                        add_payment.PaymentTypes = PaymentTypes;
                        ret_collection.Add(add_payment);
                    }
                }
            }
            return ret_collection;
        }

        ObservableCollection<Payment> readCSV()
        {
            var ret_collection = new ObservableCollection<Payment>();
            string input_dir = @"Resource";
            var inputs = Directory.GetFiles(input_dir);

            foreach (var input_path in inputs)
            {
                using (var input = new StreamReader(input_path, Encoding.UTF8))
                {
                    bool is_first = true;
                    while (input.EndOfStream == false)
                    {
                        string line = input.ReadLine();
                        if (!is_first)
                        {
                            var param = line.Split(',');
                            if (param[0].Equals("ショッピング合計"))
                            {
                                break;
                            }else if (param[0].Equals("種別（ショッピング、キャッシング、その他）"))
                            {
                                continue;
                            }
                            var payment = new Payment();
                            payment.convertDate(param[1]);
                            payment.Usage = param[2];
                            payment.Price = int.Parse(param[4]);
                            ret_collection.Add(payment);
                        }
                        is_first = false;
                    }
                }
            }

            return ret_collection;
        }

        private void outputYaml()
        {
            var root = new YamlSequenceNode();
            foreach(var payment in PaymentCollection)
            {
                var node = new YamlMappingNode();
                node.Add("Year", payment.Year.ToString());
                node.Add("Month", payment.Month.ToString());
                node.Add("Day", payment.Day.ToString());
                node.Add("Price", payment.Price.ToString());
                node.Add("Usage", payment.Usage.ToString());
                node.Add("Type", payment.Type.ToString());
                root.Add(node);
            }

            string out_path = @"Resource\yaml\CreditYaml.yml";
            YamlDocument yaml_document = new YamlDocument(root);
            YamlStream yaml_stream = new YamlStream(yaml_document);
            using(var writer = new StreamWriter(out_path, false, Encoding.UTF8)){
                yaml_stream.Save(writer, false);
            }
        }
        private void reload()
        {
            PaymentCollection = readYML();
        }
    }
}
